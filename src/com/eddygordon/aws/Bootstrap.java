package com.eddygordon.aws;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AttachVolumeRequest;
import com.amazonaws.services.ec2.model.AttachVolumeResult;
import com.amazonaws.services.ec2.model.CreateSnapshotRequest;
import com.amazonaws.services.ec2.model.CreateSnapshotResult;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.DescribeAvailabilityZonesResult;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Volume;
import com.amazonaws.services.ec2.model.VolumeAttachment;
import com.amazonaws.services.opsworks.AWSOpsWorks;
import com.amazonaws.services.opsworks.AWSOpsWorksClient;
import com.amazonaws.services.opsworks.model.CreateInstanceRequest;
import com.amazonaws.services.opsworks.model.CreateInstanceResult;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;

public class Bootstrap {
	static AmazonEC2 ec2;
	static AmazonS3 s3;
	static AmazonSimpleDB sdb;
	static AWSOpsWorks ops;
	static Properties e3Properties;

	enum PROPERTY_KEY {
		MASTER_EBS_VOLUME
	}

	private static void init() throws Exception {
		/*
		 * This credentials provider implementation loads your AWS credentials from a properties file at the root of your classpath.
		 */
		AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();

		ec2 = new AmazonEC2Client(credentialsProvider);
		
		s3 = new AmazonS3Client(credentialsProvider);
		sdb = new AmazonSimpleDBClient(credentialsProvider);
		ops = new AWSOpsWorksClient(credentialsProvider);
		e3Properties = loadProperties();
	}

	public static void main(String[] args) throws Exception {

		System.out.println("===========================================");
		System.out.println("Welcome to the AWS Java SDK!");
		System.out.println("===========================================");

		init();

		try {
//			DescribeAvailabilityZonesResult availabilityZonesResult = ec2.describeAvailabilityZones();
//			System.out.println("You have access to " + availabilityZonesResult.getAvailabilityZones().size() + " Availability Zones.");

//			DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
//			List<Reservation> reservations = describeInstancesRequest.getReservations();
//			Set<Instance> instances = new HashSet<Instance>();

//			for (Reservation reservation : reservations) {
//				instances.addAll(reservation.getInstances());
//			}

//			System.out.println("You have " + instances.size() + " Amazon EC2 instance(s) running.");

			DescribeVolumesResult describeVolumesResult = ec2.describeVolumes();

			Set<Volume> volumes = new HashSet<Volume>();
			for (Volume volume : describeVolumesResult.getVolumes()) {
				volumes.add(volume);
				if (volume.getVolumeId().equals(e3Properties.getProperty(PROPERTY_KEY.MASTER_EBS_VOLUME.toString()))) {
					System.out.println("creating ebs snapshot for master volume " + volume.getVolumeId());
					createSnapshot(volume, new PostProcessor() {
						@Override
						public void doWork(Object snapshotId) {
							createVolumeFromSnapshot(snapshotId, new PostProcessor() {
								
								@Override
								public void doWork(Object volumeId) {
									CreateInstanceRequest createInstanceRequest = new CreateInstanceRequest();
									createInstanceRequest.setAmiId(e3Properties.getProperty("MASTER_IMAGE"));
									
									CreateInstanceResult createInstanceResult = ops.createInstance(createInstanceRequest);
									System.out.println("FINISHED CREATING INSTANCE instanceid:"+createInstanceResult.getInstanceId());
									
									AttachVolumeRequest attachVolumeRequest = new AttachVolumeRequest();
									attachVolumeRequest.setInstanceId(createInstanceResult.getInstanceId());
									attachVolumeRequest.setVolumeId(volumeId.toString());
									AttachVolumeResult attachVolumeResult = ec2.attachVolume(attachVolumeRequest);
									do{
										try {
											Thread.sleep(1000);
										} catch (InterruptedException exception) {
											exception.printStackTrace();
										}
										
										if(attachVolumeResult.getAttachment().getState().equals("completed"))
											break;
									}while(true);
									System.out.println("ATTACHED VOLUME TO INSTANCE");
									
									System.out.println("COMPLETED ALL OPERATIONS");
								}
							});
						}

						private void createVolumeFromSnapshot(Object snapshotId, PostProcessor postProcessor) {
							CreateVolumeRequest createVolumeRequest = new CreateVolumeRequest(snapshotId.toString(), e3Properties
									.getProperty("MASTER_AVAILABILITY_ZONE"));
							CreateVolumeResult createVolumeResult = ec2.createVolume(createVolumeRequest);
							do{
								if(createVolumeResult.getVolume().getState().equals("completed")){
									break;
								}
								try {
									Thread.sleep(1000);
								} catch (InterruptedException exception) {
									exception.printStackTrace();
								}
							}while(true);
							System.out.println("FINISHED CREATING VOLUME..."+createVolumeResult.getVolume().getVolumeId());
							postProcessor.doWork(createVolumeResult.getVolume().getVolumeId());
						}
					});

				}
			}

			System.out.println("You have " + volumes.size() + " volumes.");

		} catch (AmazonServiceException ase) {
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
		}

	}

	public static Properties loadProperties() {
		InputStream inputStream = Bootstrap.class.getResourceAsStream("/e3.properties");
		if (inputStream == null) {
			throw new AmazonClientException("Missing e3 properties");
		}

		Properties e3Properties = new Properties();
		try {
			e3Properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (Exception e) {
				// ignore
			}
		}
		return e3Properties;
	}

	private static void createSnapshot(Volume volume, PostProcessor postProcessor) throws InterruptedException {
		CreateSnapshotRequest createSnapshotRequest = new CreateSnapshotRequest(volume.getVolumeId(), "MASTER_SNAPSHOT" + (new Date()));
		final CreateSnapshotResult createSnapshotResult = ec2.createSnapshot(createSnapshotRequest);
		do {
			if (createSnapshotResult.getSnapshot().getState().equals("completed")){
				break;
			}
			Thread.sleep(1000);
		} while (true);
		System.out.println("COMPLETED SNAPSHOT......."+createSnapshotResult.getSnapshot().getSnapshotId());
		postProcessor.doWork(createSnapshotResult.getSnapshot().getSnapshotId());
	}
}

interface PostProcessor {
	public void doWork(Object param);

}
